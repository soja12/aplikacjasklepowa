class ClientsController < ApplicationController


    def index
        @clients = Client.all
    end

    def show
        @client= Client.find(params[:id])
    end

    def new
        @client= Client.new
    end

    def create
@client = Client.new(client_params)        
        
        if @client.save
            redirect_to clients_new_path
        else
            redirect_to clients_new_path
    end
end
    def edit
        @client= Client.find(params[:id])
    end

    def update
        @client= Client.find(params[:id])

        if @client.update(client_params)
            redirect_to client_path
        else
            redirect_to clients_new_path
    end
end

    def destroy
        @client= Client.find(params[:id])

        @client.destroy
            redirect_to client_path, :notice => "client has been deleted"
        
            
         
     end

    private
   def client_params
    params.require(:client).permit(:client_id, :name,:adress, :phone_number, :comment, :order_id)

   
    end

end
    
