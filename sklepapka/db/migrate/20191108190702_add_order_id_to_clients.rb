class AddOrderIdToClients < ActiveRecord::Migration
  def change
    add_column :clients, :order_id, :integer
  end
end
